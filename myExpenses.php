<?php
include_once('config.php');
include_once("functions.php");
session_start();
//var_dump($_SESSION);
$db = new dbQueries($link);
$totalActiveExpenses = $db->allExpenseTotal();
$userExpenses = $db->UserExpenseTotal($_SESSION['id']);
$loggedUserDetails = $db->getLoggedUserDetails($_SESSION['id']);
?>

<html>
    <head></head>
    <body>
        <div class="row jumbotron">

            <div class="container">
                <div class="col-xs-5">
                    <p>Active Expenses</p>
                    <p>Active Expenses By You</p>
                </div>
                <div class="col-xs-1">
                    <p>:</p>
                    <p>:</p>
                </div>
                <div class="col-xs">
                    <p><?php echo $totalActiveExpenses ?></p>
                    <p><?php echo $userExpenses ?> </p>



                </div>
            </div>
        </div>
        <div class = "active">

            <table class="table table-bordered editableTable" >
                <thead>
                    <tr>
                        <th class="hide">Id</th>
                        <th>Date</th>
                        <th>Category</th>
                        <th>Description</th>
                        <th>Amount</th>
                        <th>Paid By</th>
                    </tr>
                </thead>
                <tbody>

                    <?php while ($expense = mysqli_fetch_assoc($loggedUserDetails)) { ?>
                        <tr class="expenseRec" id="<?php echo $expense ['id']; ?>">

                            <td class="hide" name="id" ><?php echo $expense ['id']; ?></td>
                            <td name="date"><?php echo $expense ['date']; ?></td>
                            <td name= "catg" ><?php echo $expense ['catg']; ?></td>
                            <td name="desc"><?php echo $expense ['description']; ?></td>
                            <td name="amount"><?php echo $expense ['amount']; ?></td>
                            <td name="paid_by"><?php echo $expense ['paid_by']; ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </body>

</html>

