$(document).ready(function() {
    // below function calls the seteditable method to set the structure of the table
    $('.editableTable').SetEditable({

    });


    $('.navbar-nav').on('click', '.nav-item', function(e) {

        e.preventDefault();
        $('.active').removeClass('active');
        $(this).addClass('active');

        var c = $(this).attr('name');
        //alert(c);

        if (c == "home") {
            //$(".variableDiv").html("<?php include_once('home.php');");
            //need a better way
            window.location.replace("./welcome.php");
            //$(".variableDiv").load("home.php");
        }
        if (c == "myExp") {
            $(".variableDiv").load("myExpenses.php");
        }

        if (c == "rpt") {
            $(".variableDiv").load("report.php");
        }
        if (c == "archive") {
            $(".variableDiv").load("archive.php");
        }
        //alert(c);

    });


}

);