<?php

include_once('config.php');

class dbQueries {

    function __construct($link) {
        $this->link = $link;
    }

    function getExpenses() {

        $sqlQuery = "SELECT eh.id, eh.date, ec.catg_name as catg,eh.deleted, eh.description,eh.amount,u.username AS paid_by"
                . " FROM expense_hist eh"
                . " JOIN"
                . " users u"
                . " ON"
                . " u.id=eh.paid_by"
                . " JOIN"
                . " expense_catg ec"
                . " ON"
                . " ec.id=eh.catg"
                . " where eh.deleted=0"
                . " ORDER BY eh.id DESC";

        $resultSet = mysqli_query($this->link, $sqlQuery) or die("database error:" . mysqli_error($this->link));
        return $resultSet;
    }

    function getCatgNames() {
        $catgDropDown = "Select catg_name from expense_catg";
        $categories = mysqli_query($this->link, $catgDropDown) or die("database error:" . mysqli_error($this->link));

        return $categories;
    }

    function getUsers() {
        $users = "Select username from users";


        $userSet = mysqli_query($this->link, $users) or die("database error:" . mysqli_error($this->link));
        return $userSet;
    }

    function getLoggedUserDetails($user) {

        $sqlUserBased = "SELECT eh.id, eh.date, ec.catg_name as catg,eh.deleted, eh.description,eh.amount,u.username AS paid_by"
                . " FROM expense_hist eh"
                . " JOIN"
                . " users u"
                . " ON"
                . " u.id=eh.paid_by"
                . " JOIN"
                . " expense_catg ec"
                . " ON"
                . " ec.id=eh.catg"
                . " where eh.deleted=0"
                . " and paid_by=" . $user
                . " ORDER BY eh.id DESC";

        //echo $sqlUserBased;

        $loggedUserDetails = mysqli_query($this->link, $sqlUserBased) or die("database error:" . mysqli_error($this->link));

        return $loggedUserDetails;
    }

    function allExpenseTotal() {
        $query1 = "SELECT sum(amount) as sum from expense_hist where deleted=0 and rpt_id is NULL";

        $allExpenses = mysqli_query($this->link, $query1) or die("database error:" . mysqli_error($this->link));
        $result = mysqli_fetch_row($allExpenses);
        $sumTotal = $result[0];

        return $sumTotal;
    }

    function UserExpenseTotal($user) {
        $query = "SELECT sum(amount) from expense_hist where paid_by=" . $user . " and deleted=0 and rpt_id is NULL";

        $UserExpenses = mysqli_query($this->link, $query) or die("database error:" . mysqli_error($this->link));
        $result = mysqli_fetch_row($UserExpenses);
        $sumTotalUser = $result[0];

        return $sumTotalUser;
    }

    function expenseDivision() {

        $query = "SELECT sum(amount) as sum from expense_hist where deleted=0 and rpt_id is NULL";
        $query2 = "SELECT count(*) from users where status=0";

        $allExpenses = mysqli_query($this->link, $query) or die("database error:" . mysqli_error($this->link));
        $result = mysqli_fetch_row($allExpenses);
        $sumTotal = $result[0];

        $UserCountObj = mysqli_query($this->link, $query2) or die("database error:" . mysqli_error($this->link));
        $result2 = mysqli_fetch_row($UserCountObj);
        $totalActiveUsers = $result2[0];



        $eachPays = $sumTotal / $totalActiveUsers;

        $eachPays = (string) $eachPays;

        $ExpArr = ["ActiveUsers" => $totalActiveUsers, "EachPays" => $eachPays];

        return $ExpArr;
    }

    function YouPay($expensesDone, $eachPays) {
        $rpt_status = array();
        if ($expensesDone > $eachPays) {

            $AR = $expensesDone - $eachPays;
            $AR = (string) $AR;
            $rpt_status = ["status" => "Receivable", "Amount" => $AR];
            return $rpt_status;
        } else if ($expensesDone < $eachPays) {
            $AP = $eachPays - $expensesDone;
            $AP = (string) $AP;
            $rpt_status = ["status" => "Payable", "Amount" => $AP];
            return $rpt_status;
        } else if ($expensesDone == $eachPays) {
            $rpt_status = ["status" => "No Actions Needed", "Amount" => "Report Can Be Closed !!!"];
            return $rpt_status;
        }
    }

}

?>