//var params = null;  		//Parameters
//column numbers that should be looked for new content
var colsEdi = null;
var newColHtml = '<div class="btn-group pull-right">' +
        '<button id="bEdit" type="button" class="btn btn-sm btn-default" onclick="rowEdit(this);">' +
        '<span class="glyphicon glyphicon-pencil" > </span>' +
        '</button>' +
        '<button id="bElim" type="button" class="btn btn-sm btn-default" onclick="rowElim(this);">' +
        '<span class="glyphicon glyphicon-trash" > </span>' +
        '</button>' +
        '<button id="bAcep" type="button" class="btn btn-sm btn-default" style="display:none;" onclick="EdittedRow(this);">' +
        '<span class="glyphicon glyphicon-ok" > </span>' +
        '</button>' +
        '<button id="bCanc" type="button" class="btn btn-sm btn-default" style="display:none;" onclick="rowCancel(this);">' +
        '<span class="glyphicon glyphicon-remove" > </span>' +
        '</button>' +
        '</div>';

var addExpHTML = '<div class="btn-group pull-right">' +
        '<button id="bAddExp" type="button" class="btn btn-sm btn-default" onclick="addExp(this);">' +
        '<span class="glyphicon glyphicon-plus" > </span>' +
        '</button>' +
        '</div>';


var colEdicHtml = '<td name="buttons">' + newColHtml + '</td>';
var colAddHTML = '<td name="buttonsAdd">' + addExpHTML + '</td>'


$.fn.SetEditable = function(options) {

//edit the addition button
    this.find('thead tr').append('<th name="buttons"></th>'); //empty header
    if (this.find('tbody tr.addExp')) {
        this.find('tbody tr.addExp').append(addExpHTML);
    }
    if (this.find('tbody tr.expenseRec')) {
        this.find('tbody tr.expenseRec').append(colEdicHtml);
    }

};
function EditRow($cols, tarea) {
//Iterates through the editable fields in a row
    var n = 0;
    $cols.each(function() {
        n++;
        if ($(this).attr('name') == 'buttons')
            return; //exclude button column
        if (!EsEditable(n - 1))
            return; //not editable
        tarea($(this));
    });
    function EsEditable(idx) {
        //Indicates if the passed column is configured to be editable
        if (colsEdi == null) {  //was not defined
            return true; //all are editable
        } else {  //there is a field filter

//alert ('checking:' + idx);
            for (var i = 0; i < colsEdi.length; i++) {
                if (idx == colsEdi[i])
                    return true;
            }
            return false; //wasnt found
        }
    }
}
function SetNormalMode(but) {
    $(but).parent().find('#bAcep').hide();
    $(but).parent().find('#bCanc').hide();
    $(but).parent().find('#bEdit').show();
    $(but).parent().find('#bElim').show();
    var $row = $(but).parents('tr'); //access the row
    $row.attr('id', ''); //remove mark
}
function SetEditMode(but) {
    $(but).parent().find('#bAcep').show();
    $(but).parent().find('#bCanc').show();
    $(but).parent().find('#bEdit').hide();
    $(but).parent().find('#bElim').hide();
    var $row = $(but).parents('tr'); //queue
    $row.attr('id', 'editing'); //access the row
}
function EditMode($row) {
    if ($row.attr('id') == 'editing') {
        return true;
    } else {
        return false;
    }
}
function EdittedRow(but) {

//Accept edit changes

    var $row = $(but).parents('tr'); //access the row
    var $cols = $row.find('td'); //read fields

    if (!EditMode($row))
        return; //It is already in edition mode

    //It is in edition. We have to finish the edition
    var myVar = {};
    EditRow($cols, function($td) {  //iterate through the columns

        var name = $td.attr('name');
        if (name == "id" || name == "paid_by") {
            myVar[name] = $td.html();
            return;
        }

        var cont = "";
        if (undefined !== $td.find('input').val()) {
            cont = $td.find('input').val();
        } else if (undefined !== $td.find('.categories').val()) {
            cont = $td.find('.categories').val();
        }
        myVar[name] = cont;
        $td.html(cont); //pin content and remove controls
    });
    SetNormalMode(but);
    //params.onEdit($row);

    var userData = JSON.stringify(myVar);
    $.ajax({
        url: 'action.php',
        type: 'POST',
        data: {userData, action: 'edit'},
        success: function(myVar) {
            console.log(myVar);
        }
    });
}

function rowEdit(but) {

//Start editing a row
    var $row = $(but).parents('tr'); //access the row
    var $cols = $row.find('td'); //read fields
    if (EditMode($row))  //Edit mode
        return; //It is already in edition
    //

    //Puts in edit mode
    EditRow($cols, function($td) {  //iterate through the columns

        var elem = $td.attr('name'); //get td name

        if (elem === "desc" || elem === "amount") {
            var cont = $td.html(); //read content
            var div = '<div style="display: none;">' + cont + '</div>'; //save content
            var input = '<input class="form-control input-sm"  value="' + cont + '">';
            $td.html(div + input); //fix content
        }

        //edit date changing type to date for input
        else if (elem === "date") {
            var cont = $td.html(); //read content
            var div = '<div style="display: none;">' + cont + '</div>'; //save content
            var input = '<input type="date" class="form-control input-sm"  value="' + cont + '">';
            $td.html(div + input); //fix content
        }

        //edit catg, changing to drop down
        else if (elem === "catg") {
            var $row = $(but).parents('tr');
            var cont = $td.html(); //read content
            var div = '<div style="display: none;">' + cont + '</div>'; //save content
            var userData = JSON.stringify(cont);

            $.ajax({
                url: 'action.php',
                type: 'POST',
                data: {userData, action: 'dbCatg'},
                success: function(data) {
                    $td.html(data);
                }
            });

        }

    });
    SetEditMode(but);
}

function addExp(but) {
    var $row = $(but).parents('tr');
    var $cols = $row.find('td');
    var myVar = {};
    $cols.each(function() {

        var formInput = $(this).find(".adder").val();
        var inputName = $(this).find(".adder").attr('name');
        //alert(inputName);
        //id column to be removed
        if (inputName == undefined) {
            return;
        }
        if (formInput != undefined && formInput != "") {
            //extra validations for input
            myVar[inputName] = formInput;
            //alert(myVar[inputName]);

        } else {
            alert("Please check your input for " + inputName + " before proceeding!");
            return false;
        }

    });

    var userData = JSON.stringify(myVar);
    $.ajax({
        url: 'addRec.php',
        type: 'POST',
        data: {userData},
        success: function(myVar) {
            console.log(myVar);
            location.reload();
        }
    });
}


function rowElim(but) {  //Delete the current row
    var $row = $(but).parents('tr'); //accede row
    var id = $row.find("td[name='id']").html();

    $row.remove();


    var userData = JSON.stringify(id);
    $.ajax({
        url: 'action.php',
        type: 'POST',
        data: {userData, action: 'delete'},
        success: function(myVar) {
            console.log(myVar);
        }
    });
}
