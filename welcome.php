<?php
// Initialize the session
session_start();

// check if the user is logged in, if not then redirect him to login page
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
    header("location: login.php");
    exit;
}



//var_dump($loggedUserDetails);
//die();
//var_dump($_SESSION);
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Welcome</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"s>

        <link rel="stylesheet" href="myStyles.css">

        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="bootstable.js"></script>

        <script type="text/javascript" src="editable.js"></script>

    </head>
    <body>



        <div class="page-header" id="welcome_page">
            <h1>Hi, <b><?php echo htmlspecialchars($_SESSION["username"]); ?></b>! Welcome to your Expense Manager.</h1>

            <p>
                <a href="reset-password.php" class="btn btn-warning">Reset Your Password</a>
                <a href="logout.php" class="btn btn-danger">Sign Out of Your Account</a>
            </p>

        </div>

        <div>
            <nav class="navbar navbar-expand-sm navbar-light bg-light">
                <div class="navbar-nav">
                    <a class="nav-item nav-link active" name="home" href="./welcome.php">Home <span class="sr-only">(current)</span></a>
                    <a class="nav-item nav-link" name="myExp" href="#">My Expenses</a>
                    <a class="nav-item nav-link" name = "rpt" href="#">Report</a>
                    <a class="nav-item nav-link"  name="archive" href="#">ARCHIVE</a>
                </div>
        </div>
    </nav>
</div>

<br><br><br>

<div class="variableDiv">

    <?php include_once('home.php'); ?>

</div>



<br>
<br>
<br>

<!--<div class="footer"><p>&copy NAYEEM HASAN</p></div> -->

</body>
</html>
