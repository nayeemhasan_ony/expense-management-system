<?php
include_once('config.php');
include_once('functions.php');

session_start();
$db = new dbQueries($link);
$totalActiveExpenses = $db->allExpenseTotal();
$userExpenses = $db->UserExpenseTotal($_SESSION['id']);
$loggedUserDetails = $db->getLoggedUserDetails($_SESSION['id']);
$ExpArr = $db->expenseDivision();
$YouPay = $db->YouPay($userExpenses, $ExpArr['EachPays']);
?>

<html>

    <head>

        <link rel="stylesheet" href="myStyles.css">

    </head>

    <body>

        <div class="row jumbotron">

            <div class="container">
                <div class="col-xs-5">
                    <p>Active Expenses</p>
                    <p>Active Expenses By You</p>
                    <p>Total Users</p>
                    <p>Each Pays</p>
                    <p>-------------------------</p>
                    <p style="background-color:tomato;"><?php echo $YouPay['status'] ?></p>
                </div>
                <div class="col-xs-1">
                    <p>:</p>
                    <p>:</p>
                    <p>:</p>
                    <p>:</p>
                    <p>---</p>
                    <p>:</p>
                </div>
                <div class="col-xs">
                    <p><?php echo $totalActiveExpenses ?></p>
                    <p><?php echo $userExpenses ?> </p>
                    <p><?php echo $ExpArr['ActiveUsers'] ?></p>

                    <p><?php echo $ExpArr['EachPays'] ?></p>
                    <p><?php echo " -------" ?></p>
                    <p><?php echo $YouPay['Amount'] ?></p>



                </div>
            </div>
        </div>



    </body>



</html>
